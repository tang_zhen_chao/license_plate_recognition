# License Plate Recognition

#### Introduction
opencv digital image processing, pca + svm license plate recognition

#### Description
This small project is for my practice to learn the support vector machine algorithm, but following the SVM theory, more difficult objects are in digital image processing, such as license plate detection, character segmentation, but they have been resolved under persistence. Hope Everyone can join together to experience machine learning and digital image processing
License plate recognition process:
1 License plate detected
2 Segment the characters of the license plate
3 character recognition using SVM model one by one
The specific principle (including the SVM hyperplane derivation formula) is located in the folder "theory", the format is word

#### Installation tutorial
python3
Just install the required libraries:
numpy
pandas
opencv
scikit-learn

eg: pip install scikit-learn -i https://pypi.tuna.tsinghua.edu.cn/simple


#### Instructions for use
First, you can directly run demo.py to directly view the results of license plate recognition
As a study, you can participate step by step:
1.SVM: pixeltocsv writes training pictures to csv
          svmtrain trains the svm model, which uses pca for dimension reduction, and the model is saved in .m format
          loadsvm as an interface for using the model
2.locate, license plate positioning: Gaussian filter, median filter;
         Sobel operator edge detection, binarization;
         Expansion once, corrosion once;
         Find the contour according to the aspect ratio range of the license plate, and cut out the contour
3.spli, character segmentation: repeated filtering to obtain binary images without impurities (vertical projection discontinuous),
                Divide characters according to pulse signals of character pixels
4. Characters are recognized one by one using SVM


#### demo results
![origin](https://images.gitee.com/uploads/images/2020/0511/165555_89cf2c56_5644878.png "car.png")
![char](https://images.gitee.com/uploads/images/2020/0511/165611_9e59a9f4_5644878.png "char.png")
![result](https://images.gitee.com/uploads/images/2020/0511/165625_9d6d5554_5644878.png "result.png")
