# 车牌识别

#### 介绍
opencv数字图像处理，pca+svm车牌识别

#### 说明
该小工程是我为了学习支持向量机算法作为练习的，但继SVM的理论后，更多的困难体在数字图像处理上，比如车牌检测，字符分割，不过在坚持下都已被解决，希望大家能加入一起体验机器学习与数字图像处理
车牌识别的流程：
1检测到车牌
2将车牌的字符分割出来
3字符逐个使用SVM模型识别
具体的原理（包括SVM超平面推导公式）位于文件夹“theory”，格式为word

#### 安装教程
python3
安装需要的库即可：
numpy
pandas
opencv
scikit-learn

eg:pip install scikit-learn -i https://pypi.tuna.tsinghua.edu.cn/simple


#### 使用说明
首先可以直接运行demo.py，直接查看车牌识别的结果
作为学习，大家可以一步一步参与：
1.SVM：pixeltocsv将训练图片写入csv
          svmtrain训练svm模型，其中用pca进行降维，模型保存为.m格式
          loadsvm作为使用模型的接口
2.locate，车牌定位：高斯滤波，中值滤波；
         Sobel算子边缘检测，二值化；
         膨胀一次，腐蚀一次；
         根据车牌长高比范围查找轮廓，将轮廓裁剪出来
3.spli，字符分割：反复滤波获得不含杂质（垂直投影不连续）的二值图像，
                根据字符像素的脉冲信号起止分割字符
4.字符逐个使用SVM识别

注意如果是opencv版本差异带来的问题：
image_process,contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
改为
contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)即可
由于训练集只有数字和英文，所以不能识别车牌的中文字符

#### demo结果
![原图](https://images.gitee.com/uploads/images/2020/0511/165422_c8fcc88f_5644878.png "car.png")
![字符分割](https://images.gitee.com/uploads/images/2020/0511/165445_0ba2dcaf_5644878.png "char.png")
![识别结果](https://images.gitee.com/uploads/images/2020/0511/165500_818174fe_5644878.png "result.png")

