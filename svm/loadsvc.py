from sklearn.externals import joblib
import cv2 as cv

def load_model(svc_path,pca_path):
    svc = joblib.load(svc_path)
    pca = joblib.load(pca_path)
    return svc,pca

def nameindex(pre):
    dir=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C',
         'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
         'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    for i in range(len(dir)):
        if (pre==i):
            return dir[i]

if __name__ =="__main__":
    img = cv.imread("./varchar/9.png", 0)
    img = cv.resize(img, (20, 20), interpolation=cv.INTER_CUBIC)
    test = img.reshape(1,400)
    #加载模型
    svc,pca=load_model("./model.m","./pca.m")
    # svm
    print('start pca...')
    test_x = pca.transform(test)
    print(test_x.shape)
    pre = svc.predict(test_x)
    print(pre)