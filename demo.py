import locate.locateplate as locateplate
import split.splitarea as splitarea
import svm.loadsvc as loadsvc
import cv2

class Demo():
    def __init__(self,img_path,svc_path,pca_path):
        self.img_path=img_path
        self.svc_path=svc_path
        self.pca_path=pca_path
    def run(self):
        #车牌定位
        img = cv2.imread(self.img_path)
        cv2.imshow("car",img)
        plate=locateplate.detect(img)
        #字符分割
        binary,dilation=splitarea.preprocess(plate)
        thresh = splitarea.pickpoint(dilation)
        charlist = splitarea.cut(thresh,binary)
        #加载svm模型
        svc, pca = loadsvc.load_model(self.svc_path,self.pca_path)
        string=[]
        #每个字符进行识别
        for i in range(len(charlist)):
            name="result{}".format(str(i))
            cv2.imshow(name,charlist[i])
            #没有中文数据集，所以不进行识别中文
            if(i==0):
                continue
            char=cv2.resize(charlist[i], (20, 20), interpolation=cv2.INTER_CUBIC)
            test=char.reshape(1,400)
            test_x = pca.transform(test)
            pre = svc.predict(test_x)
            string.append(loadsvc.nameindex(pre))
        print("车牌非中文字符：",string)
        cv2.waitKey(0)

if __name__ == '__main__':
    img_path="./valimg/car.jpg"
    svc_path="./svm/model.m"
    pca_path="./svm/pca.m"
    platedemo=Demo(img_path,svc_path,pca_path)
    platedemo.run()